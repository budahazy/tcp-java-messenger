package messenger;

public interface MessengerModel {

    boolean connect(final String host, final int port);

    void send(final String message);

    void close();

}
