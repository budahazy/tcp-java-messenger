package messenger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ModelTCP implements MessengerModel {

    ListenerModel listener;
    ExecutorService executor;
    Future<Void> future;
    volatile boolean timeToStop;
    InetSocketAddress socketAddress;
    Socket senderSocket;
    Socket reciverSocket;
    ServerSocket server;

    public ModelTCP(ListenerModel listener, int port) {
        this.listener = listener;
        this.timeToStop = false;
        this.senderSocket = new Socket();
        try {
            this.server = new ServerSocket(port);
            this.server.setSoTimeout(1000);
            this.executor = Executors.newCachedThreadPool();
            this.future = executor.submit(new MessageListener());
        } catch (IOException ex) {
            listener.errorHappened(ex.getMessage());
        }
    }

    @Override
    public boolean connect(String host, int port) {
        try {
            this.socketAddress = new InetSocketAddress(host, port);
            senderSocket.connect(socketAddress);
            senderSocket.shutdownInput();
            return true;
        } catch (IOException ex) {
            listener.errorHappened(ex.getMessage());
            return false;
        }
    }

    @Override
    public void send(String message) {
        try {
            byte[] messageData = message.getBytes(StandardCharsets.UTF_8);
            OutputStream outputStream = senderSocket.getOutputStream();
            outputStream.write(messageData);
            outputStream.flush();
        } catch (IOException ex) {
            listener.errorHappened(ex.getMessage());
        }
    }

    @Override
    public void close() {
        this.timeToStop = true;
        try {
            future.get();
            this.executor.shutdown();
            this.senderSocket.close();
            if (reciverSocket != null) {
                this.reciverSocket.close();
            }
            this.server.close();
        } catch (InterruptedException | ExecutionException ex) {
            listener.errorHappened(ex.getMessage());
        } catch (IOException ex) {
            listener.errorHappened(ex.getMessage());

        }

    }

    private class MessageListener implements Callable<Void> {

        byte[] recivedMessage;
        int recivedMessageLength;

        public MessageListener() {

        }

        @Override
        public Void call() throws Exception {
            recivedMessage = new byte[2000000];
            InputStream inputStream = null;
            while (!timeToStop) {
                try {
                    reciverSocket = server.accept();
                    inputStream = reciverSocket.getInputStream();
                    reciverSocket.shutdownOutput();
                    listener.messageRecived("Someone Connected");
                    break;
                } catch (SocketTimeoutException ex) {
                    continue;
                } catch (IOException ex) {
                    listener.errorHappened(ex.getMessage());
                }
            }
            while (!timeToStop) {
                System.err.println("while");
                try {
                    recivedMessageLength = inputStream.read(recivedMessage);
                } catch (SocketTimeoutException ex) {
                    continue;
                }
                if (recivedMessageLength == -1) {
                    break;
                } else {
                    String message = new String(recivedMessage, 0, recivedMessageLength, StandardCharsets.UTF_8);
                    listener.messageRecived(message);
                }
            }

            return null;
        }

    }

}
