package messenger;

import java.util.List;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class View extends Application implements ListenerModel {

    ModelTCP model;
    TextArea textAreaMessages;

    int port = 8000;

    @Override
    public void start(Stage primaryStage) {

        StackPane root = new StackPane();
        Scene scene = new Scene(root, 500, 500);
        GridPane grid = new GridPane();
        root.getChildren().add(grid);

        TextField textFieldAddress = new TextField("address");
        TextField textFieldPort = new TextField("port");
        Button buttonConnect = new Button("Connect");
        buttonConnect.setOnAction((ActionEvent event) -> {
            if (model.connect(textFieldAddress.getText(), Integer.valueOf(textFieldPort.getText()))) {
                textAreaMessages.setText(textAreaMessages.getText() + " -> " + "Connection successful");
            }
        });
        textAreaMessages = new TextArea();
        textAreaMessages.setEditable(false);
        TextField textFieldMessage = new TextField("Your message");
        Button buttonSendMsg = new Button("Send!");
        buttonSendMsg.setOnAction((ActionEvent event) -> {
            model.send(textFieldMessage.getText());
            textAreaMessages.setText(textAreaMessages.getText() + "\n <-" + textFieldMessage.getText());
        });

        grid.add(textFieldAddress, 0, 0);
        grid.add(textFieldPort, 1, 0);
        grid.add(buttonConnect, 2, 0);
        grid.add(textAreaMessages, 0, 1);
        grid.add(textFieldMessage, 0, 2);
        grid.add(buttonSendMsg, 1, 2);

        List<String> args = getParameters().getRaw();

        if (!args.isEmpty()) {
            port = Integer.parseInt(args.get(0));
        }

        primaryStage.setOnShowing(event -> {
            model = new ModelTCP(this, port);
        });
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void stop() {
        model.close();
    }

    @Override
    public void messageRecived(String message) {
        textAreaMessages.setText(textAreaMessages.getText() + "\n -> " + message);
    }

    @Override
    public void errorHappened(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(message);
        alert.showAndWait();
    }

}
